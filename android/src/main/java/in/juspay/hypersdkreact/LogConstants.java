/*
 * Copyright (c) Juspay Technologies.
 *
 * This source code is licensed under the AGPL 3.0 license found in the
 * LICENSE file in the root directory of this source tree.
 */

package in.juspay.hypersdkreact;

final class LogConstants {
    public static final String SUBCATEGORY_HYPER_SDK = "hypersdk";
    public static final String LEVEL_INFO = "info";
    public static final String LEVEL_ERROR = "error";
    public static final String SDK_TRACKER_LABEL = "hyper_sdk_react";
}
